ros by example 学习笔记
============================
> 目录
<!-- TOC -->

- [1. PURPOSE OF THIS BOOK](#1-purpose-of-this-book)
- [2. Real and Simulated Robots](#2-real-and-simulated-robots)
- [3. Operating Systems and ROS Versions](#3-operating-systems-and-ros-versions)
- [4. Reviewing the ROS Basics](#4-reviewing-the-ros-basics)

<!-- /TOC -->

# 1. PURPOSE OF THIS BOOK
&ensp;&ensp;&ensp;&ensp;ROS is extremely powerful and continues to expand and improve at a rapidly accelerating pace. Yet one of the challenges facing many new ROS users is knowing where to begin. There are really two phases to getting started with ROS: Phase 1 involves learning the basic concepts and programming techniques while Phase 2 is about using ROS to control your own robot

# 2. Real and Simulated Robots
&ensp;&ensp;&ensp;&ensp;While ROS can be used with a large variety of hardware, you don't need an actual robot to get started. ROS includes packages to run a number of robots in simulation so that you can test your programs before venturing into the real world.

# 3. Operating Systems and ROS Versions

# 4. Reviewing the ROS Basics